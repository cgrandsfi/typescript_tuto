List of init command
--------------------


1) init new project with typescript

(source https://github.com/nvm-sh/nvm#installing-and-updating)
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
nvm install --lts
nvm use --use-lts
npm -g install yarn

## go to your projects directory
cd www

yarn create vite typescript_tuto
## select vanilla and typescript
cd typescript_tuto
yarn
yarn dev

git init
## add git remote
git remote add origin git@gitlab.com:cgrandsfi/typescript_tuto.git


git add .
git commit -m "initial commit"
## push to remote
git push --set-upstream origin main
```


Eviter 
- ```any```
- ```as``` car le typage déclaré peut ne pas correspondre au typage réel exemple :
```
let aa = document.querySelector('#azeaze') as HTMLDivElement;// le type de aa devient HTMLDivElement ce qui est incorrect
let bb = document.querySelector<HTMLDivElement>('#azeaze');// le type de bb devient HTMLDivElement|null ce qui est correct
```
- du coup interdit d'utiliser ```!```




```yarn watch```  ne vérifie pas le typage (le développeur est sensé suivre les erreurs de l'ide)
par contre ```yarn build``` le fait ( pour s'assurer que tout va bien avant l'execution)


préférer vscode parce ce que phpstorm ne renvoie toujours correctement les erreurs de typage
exemple :
```
document.querySelector<HTMLDivElement>('#elementQuiNexistePas').innerHTML = 'aaa';
```
car le type de ```document.querySelector<HTMLDivElement>('#elementQuiNexistePas')``` est de type ```HTMLDivElement|null```
 et on ne peut pas affecter une propriété ```innerHTML``` sur un élément null

